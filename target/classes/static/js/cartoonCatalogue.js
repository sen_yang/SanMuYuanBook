(function ($) {
    $.getUrlParam = function (name) {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
        var r = window.location.search.substr(1).match(reg);
        if (r != null) return unescape(r[2]); return null;
    }
})(jQuery);
$(function () {
    $.ajax({
        type: "POST",
        url: "/cartoon/getCartoonCatalogue",
        data: {"cartoonCod": $.getUrlParam('cartoonCod')},
        dataType: "json",
        success: function (data) {
            $("#bookName").html(data.cartoonName);
            let strhtml = "";
            let size=0;
            for (let i = 0; i < data.cartoonCatalogues.length; i++) {
                strhtml+= "<li data-rid=\""+(i+1)+"\"><a href='readCartoon.html?cartoonCod=" + data.cartoonCode
                    + "&&chapterCod="+data.cartoonCatalogues[i].catalogueCode+"'>" + data.cartoonCatalogues[i].catalogueName + "</a></li>";
            }

            $("#bookImg").append("<img src='"+data.cartoonImage+"'/>");
            $("#author").append(data.cartoonAuthor);
            $("#intro").append(data.cartoonIntro);
            $("#body").append(strhtml);
        },
        complete: function (data) {
        }
    });
});
function show() {
    window.location="/cartoon/downloadCartoon?cartoonCod="+$.getUrlParam('cartoonCod')+"&&bookName1="+$("#bookName").text();
}

