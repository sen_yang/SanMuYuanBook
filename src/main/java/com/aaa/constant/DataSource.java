package com.aaa.constant;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 杨森
 * @version 1.0
 * @Title:
 * @date 2020/9/17 15:46
 */
public class DataSource {
    public static Map<String, List<String>> getDataSource(){
        Map<String, List<String>> listMap=new HashMap<>();
        List<String> list=new ArrayList<>();
        list.add(0,"GET--https://www.biquge5200.com/modules/article/search.php?searchkey=");
        listMap.put("biquge5200",list);
        list = new ArrayList<>();
        list.add(0,"POST--http://www.ddxs.com/search.php");
        listMap.put("ddxs",list);
        list = new ArrayList<>();
        list.add(0,"GET--http://www.biquge.com/searchbook.php?keyword=");
        listMap.put("biquge",list);
        return listMap;
    }
}
