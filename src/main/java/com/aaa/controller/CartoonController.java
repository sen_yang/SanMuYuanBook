package com.aaa.controller;

import com.aaa.dto.CartoonCatalogueDto;
import com.aaa.entity.Cartoon;
import com.aaa.entity.CartoonCatalogue;
import com.aaa.service.CartoonService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.crypto.Data;
import java.io.File;
import java.util.List;
import java.util.UUID;

import static com.aaa.util.Download.download;

/**
 * @author 杨森
 * @version 1.0
 * @Title: 漫画获取
 * @date 2020/8/24 11:10
 */
@Api("漫画操作")
@Controller
@RequestMapping("/cartoon")
public class CartoonController {

    @Autowired
    private CartoonService cartoonService;

    private final static Logger log = LoggerFactory.getLogger(BookController.class);

    @PostMapping("/pageList")
    @ResponseBody
    @ApiOperation("获取漫画列表")
    private List<Cartoon> getCartoonList(
        @ApiParam("key") @RequestParam("key") String key,
        @ApiParam("dataSource") @RequestParam("dataSource") String cartoonData
    ){
        try{
            if(StringUtils.isEmpty(key)){
                return null;
            }if(StringUtils.isEmpty(cartoonData)){
                cartoonData="gufengmh8";
            }
            return cartoonService.getCartoonList(key,cartoonData);
        }catch (Exception e){
            e.printStackTrace();
            log.info("ERROR----url:cartoon/pageList;key=" + key + ";dataSource=" + cartoonData);
            return null;
        }
    }
    @PostMapping("/getCartoonCatalogue")
    @ResponseBody
    @ApiOperation("漫画目录")
    private CartoonCatalogueDto getCartoonCatalogue(
        @ApiParam("cartoonCod") @RequestParam("cartoonCod") String cartoonCod,
        @ApiParam("dataSource") @RequestParam("dataSource") String cartoonData
    ){
        try{
            if(StringUtils.isEmpty(cartoonCod)){
                return null;
            }if(StringUtils.isEmpty(cartoonData)){
                cartoonData="gufengmh8";
            }
            return cartoonService.getCartoonCatalogue(cartoonCod,cartoonData);
        }catch (Exception e){
            e.printStackTrace();
            log.info("ERROR----url:cartoon/getCartoonCatalogue;cartoonCod=" + cartoonCod + ";dataSource=" + cartoonData);
            return null;
        }
    }
    @PostMapping("/getCartoonChapter")
    @ResponseBody
    @ApiOperation("漫画内容")
    private CartoonCatalogue getCartoonChapter(
        @ApiParam("cartoonCod") @RequestParam("cartoonCod") String cartoonCod,
        @ApiParam("chapterCod") @RequestParam("chapterCod") String chapterCod,
        @ApiParam("dataSource") @RequestParam("dataSource") String cartoonData
    ){
        try{
            if(StringUtils.isEmpty(cartoonCod)||StringUtils.isEmpty(chapterCod)){
                return null;
            }if(StringUtils.isEmpty(cartoonData)){
                cartoonData="gufengmh8";
            }
            return cartoonService.getCartoonChapter(cartoonCod,chapterCod,cartoonData);
        }catch (Exception e){
            log.info("ERROR----url:cartoon/getCartoonChapter;cartoonCod=" + cartoonCod + ";chapterCod=" + chapterCod+";dataSource="+cartoonData);
            return null;
        }
    }

    @RequestMapping("/downloadCartoon")
    @ResponseBody
    @ApiOperation("下载漫画")
    public String downloadBook(
            @ApiParam("漫画编码") @RequestParam("cartoonCod") String cartoonCod,
            @ApiParam("漫画名称") @RequestParam("bookName1") String bookName,
            @ApiParam("dataSource") @RequestParam("dataSource") String cartoonData,
            HttpServletRequest request,
            HttpServletResponse response
    ){
        try{
            if(StringUtils.isEmpty(cartoonCod)){
                return null;
            }if(StringUtils.isEmpty(bookName)){
                String uuid=UUID.randomUUID().toString();
                bookName="下载"+uuid.replace("-","");
            }if(StringUtils.isEmpty(cartoonData)){
                cartoonData="gufengmh8";
            }
            File file=new File("\\usr\\local\\webapps\\file\\"+bookName+".zip");
            if(file.exists()){
                download("/usr/local/webapps/file/"+bookName+".zip",bookName+".zip",request,response);
                return "";
            }
            cartoonService.downloadCartoon(cartoonCod,bookName, cartoonData);
            download("/usr/local/webapps/file/"+bookName+".zip",bookName+".zip",request,response);
            return "";
        }catch (Exception e){
            log.info("ERROR----url:cartoon/downloadCartoon;cartoonCod=" + cartoonCod + ";bookName1=" + bookName+";dataSource="+cartoonData);
            return "";
        }

    }
}
