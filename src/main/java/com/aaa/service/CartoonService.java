package com.aaa.service;

import com.aaa.dto.CartoonCatalogueDto;
import com.aaa.entity.Cartoon;
import com.aaa.entity.CartoonCatalogue;

import java.util.List;

/**
 * @author 杨森
 * @version 1.0
 * @Title:
 * @date 2020/8/24 13:04
 */
public interface CartoonService {
    List<Cartoon> getCartoonList(String key,String dataSource);
    CartoonCatalogueDto getCartoonCatalogue(String cartoonCod,String dataSource);
    CartoonCatalogue getCartoonChapter(String cartoonCod,String chapterCod,String dataSource);
    void downloadCartoon(String cartoonCod,String cartoonName,String dataSource);
}
