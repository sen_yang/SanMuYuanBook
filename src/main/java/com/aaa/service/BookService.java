package com.aaa.service;

import com.aaa.dto.BookCatalogueDto;
import com.aaa.entity.Book;
import com.aaa.entity.FileBook;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author 杨森
 * @version 1.0
 * @Title: 书籍操作
 * @date 2020/7/24 15:52
 */
public interface BookService {

    /**
      * 根据书籍编码获取目录
      * @author yangsen
      * @date 2020/7/28
      * @param
      * @return
      */
    List<BookCatalogueDto> getBookCatalogue(String bookCod,String dataSource);

    void downloadBook(String bookCod,String bookName,HttpServletRequest request,String dataSource) throws Exception;

    BookCatalogueDto getBookChapter(String bookCod, String chapterCod, String dataSource);

    List<Book> getBookList(String key,String dataSource);

    List<FileBook> getBookFile(Integer page);

}
