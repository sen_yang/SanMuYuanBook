package com.aaa.service.impl;

import com.aaa.config.ThreadExecutorConfig;
import com.aaa.data.CartoonCatalogueList;
import com.aaa.data.CartoonChapter;
import com.aaa.data.CartoonList;
import com.aaa.data.DownloadCartoon;
import com.aaa.dto.CartoonCatalogueDto;
import com.aaa.entity.Cartoon;
import com.aaa.entity.CartoonCatalogue;
import com.aaa.service.CartoonService;
import com.aaa.util.GetDocument;
import org.apache.commons.codec.language.bm.Lang;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;

/**
 * @author 杨森
 * @version 1.0
 * @Title:
 * @date 2020/8/24 13:04
 */
@Service
public class CartoonServiceImpl implements CartoonService {

    @Override
    public List<Cartoon> getCartoonList(String key,String dataSource) {
        List<Cartoon> cartoons = CartoonList.setDataSource(key,dataSource);
        return cartoons;
    }

    @Override
    public CartoonCatalogueDto getCartoonCatalogue(String cartoonCod,String dataSource) {
        CartoonCatalogueDto cartoonCatalogueDto = CartoonCatalogueList.setDataSource(cartoonCod, dataSource);

        return cartoonCatalogueDto;
    }

    @Override
    public CartoonCatalogue getCartoonChapter(String cartoonCod, String chapterCod,String dataSource) {
        CartoonCatalogue cartoonCatalogue = CartoonChapter.setDataSource(cartoonCod, chapterCod, dataSource, this);
        return cartoonCatalogue;
    }

    @Override
    public void downloadCartoon(String cartoonCod,String cartoonName, String dataSource) {
        ExecutorService executorService= ThreadExecutorConfig.testFxbDrawExecutor();
        DownloadCartoon.setDataSource(cartoonCod,cartoonName,dataSource,executorService,this);
    }


}
