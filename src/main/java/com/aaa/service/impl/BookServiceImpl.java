package com.aaa.service.impl;


import com.aaa.config.ThreadExecutorConfig;
import com.aaa.data.BookCatalogueDB;
import com.aaa.data.BookChapter;
import com.aaa.data.BookList;
import com.aaa.data.DownloadBook;
import com.aaa.dto.BookCatalogueDto;
import com.aaa.entity.Book;
import com.aaa.entity.FileBook;
import com.aaa.service.BookService;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;

/**
 * @author 杨森
 * @version 1.0
 * @Title: 书籍操作
 * @date 2020/7/24 15:54
 */
@Service
public class BookServiceImpl implements BookService {

    @Override
    public List<BookCatalogueDto> getBookCatalogue(String bookCod, String dataSource) {
        ExecutorService executorService = ThreadExecutorConfig.testFxbDrawExecutor();
        List<BookCatalogueDto> bookCatalogues = BookCatalogueDB.setDataSource(dataSource, bookCod, executorService);
        return bookCatalogues;
    }

    @Override
    public void downloadBook(String bookCod, String bookName, HttpServletRequest request, String dataSource) throws Exception {
        ExecutorService executorService = ThreadExecutorConfig.testFxbDrawExecutor();
        DownloadBook.setDataSource(bookCod, bookName, dataSource, request, this, executorService);
    }


    @Override
    public BookCatalogueDto getBookChapter(String bookCod, String chapterCod, String dataSource) {
        BookCatalogueDto bookCatalogueDto = BookChapter.setDataSource(dataSource, bookCod, chapterCod);
        return bookCatalogueDto;
    }

    @Override
    public List<Book> getBookList(String key, String dataSource) {
        ExecutorService executorService = ThreadExecutorConfig.testFxbDrawExecutor();
        List<Book> books = BookList.setDataSource(dataSource, key, executorService);
        return books;
    }

    @Override
    public List<FileBook> getBookFile(Integer page) {
        int num = page * 5;
        File file = new File("/usr/local/webapps/file");
        File[] files = file.listFiles();
        if (files == null) {
            return null;
        }
        List<FileBook> fileBooks = new ArrayList<>(files.length);
        List<File> list = new ArrayList<>(Arrays.asList(files));
        int ceil = (int) Math.ceil((double) list.size() / 5);
        list = list.subList(num - 5, Math.min(num, list.size()));
        for (int i = 0; i < list.size(); i++) {
            FileBook fileBook = new FileBook();
            fileBook.setBookName(list.get(i).getName());
            fileBook.setFileSize(getFileSize(list.get(i)));
            fileBook.setPageSize(ceil);
            fileBooks.add(fileBook);
        }
        return fileBooks;
    }

    public static String getFileSize(File file) {
        String size = "";
        if (file.exists() && file.isFile()) {
            long fileS = file.length();
            DecimalFormat df = new DecimalFormat("#.00");
            if (fileS < 1024) {
                size = df.format((double) fileS) + "BT";
            } else if (fileS < 1048576) {
                size = df.format((double) fileS / 1024) + "KB";
            } else if (fileS < 1073741824) {
                size = df.format((double) fileS / 1048576) + "MB";
            } else {
                size = df.format((double) fileS / 1073741824) + "GB";
            }
        } else if (file.exists() && file.isDirectory()) {
            size = "";
        } else {
            size = "0BT";
        }
        return size;
    }

}
