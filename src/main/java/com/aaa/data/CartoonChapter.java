package com.aaa.data;

import com.aaa.config.SSLHelper;
import com.aaa.dto.CartoonCatalogueDto;
import com.aaa.entity.Cartoon;
import com.aaa.entity.CartoonCatalogue;
import com.aaa.service.CartoonService;
import com.aaa.util.GetDocument;
import org.apache.commons.codec.language.bm.Lang;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author 杨森
 * @version 1.0
 * @Title:
 * @date 2020/8/25 15:21
 */
public class CartoonChapter {
    public static void main(String[] args) {
        long start=System.currentTimeMillis();
        Jsoup.connect("http://www.hhimm.com/cool241483/1.html?s=11");
        long end=System.currentTimeMillis();
        System.out.println(end-start);
    }
    public static CartoonService cartoonService;
    public static CartoonCatalogue setDataSource(String cartoonCod,String chapterCod, String dataSource,CartoonService cartoonService) {
        CartoonChapter.cartoonService=cartoonService;
        if ("gufengmh8".equals(dataSource)||"36mh".equals(dataSource)||"wuxiamh".equals(dataSource)) {
            return gufengmh8(dataSource,cartoonCod,chapterCod);
        }
        return null;
    }
    private static CartoonCatalogue gufengmh8(String dataSource,String cartoonCod,String chapterCod) {
        CartoonCatalogue cartoonCatalogue = new CartoonCatalogue();
        List<String> list = new ArrayList<>(100);
        int i = 0;
        while (true) {
            Document document = null;
            if (i == 0) {
                document = GetDocument.getDocument("https://m."+dataSource+".com/manhua/", cartoonCod, chapterCod);
            } else {
                document = GetDocument.getDocument("https://m."+dataSource+".com/manhua/", cartoonCod, chapterCod + "-" + i);
            }
            if (i == 0) {
                if("36mh".equals(dataSource)||"wuxiamh".equals(dataSource)){
                    Elements elementsByClass = document.getElementsByClass("p10 title3");
                    cartoonCatalogue.setCatalogueName(elementsByClass.get(0).childNode(1).childNode(1).childNode(1).toString().replace("&gt;",""));
                }else{
                    Elements elementsByClass = document.getElementsByClass("BarTit");
                    cartoonCatalogue.setCatalogueName(elementsByClass.get(0).text());
                }
                CartoonCatalogueDto cartoonCatalogue1 = cartoonService.getCartoonCatalogue(cartoonCod,dataSource);
                List<CartoonCatalogue> cartoonCatalogues = cartoonCatalogue1.getCartoonCatalogues();
                cartoonCatalogues.forEach(e -> {
                    if (e.getCatalogueCode().toString().equals(chapterCod)) {
                        cartoonCatalogue.setNextCode(e.getNextCode());
                        cartoonCatalogue.setUpCode(e.getUpCode());
                    }
                });
            }
            String src = getSrc(document,dataSource);
            if (src.contains("default/cover.png")) {
                break;
            }
            list.add(src);
            i++;
        }
        getDistinctByFor(list);
        cartoonCatalogue.setCatalogueSrc(list);
        return cartoonCatalogue;
    }

    public static void getDistinctByFor(List<String> list3) {
        List<String> list = list3;
        for (int i = 0; i < list.size(); i++) {
            for (int j = i + 1; j < list.size(); j++) {
                if (list.get(i).equals(list.get(j))) {
                    list3.remove(i); //注意这里是list3
                }
            }
        }
    }

    private static String getSrc(Document document,String data) {
        Elements elementsByClass=null;
        Node childNode=null;
        if("36mh".equals(data)||"wuxiamh".equals(data)){
            elementsByClass = document.getElementsByClass("UnderPage");
            childNode = elementsByClass.get(0).childNode(5).childNode(1);
        }else if("gufengmh8".equals(data)){
            elementsByClass = document.getElementsByClass("chapter-content");
            childNode = elementsByClass.get(0).childNode(3).childNode(0);
        }
        String src = null;
        List<Node> nodes = childNode.childNodes();
        if("36mh".equals(data)){
            src= nodes.get(1).attr("src");
        }else{
            for (Node node : nodes) {
                if (node.toString().trim().contains("height=\"100%")||node.toString().trim().contains("<img")) {
                    src = node.attr("src");
                }
            }
        }
        return src;
    }
}
