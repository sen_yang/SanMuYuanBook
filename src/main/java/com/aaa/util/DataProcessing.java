package com.aaa.util;

import java.util.HashMap;
import  com.google.common.collect.Lists;
import java.util.List;
import java.util.Map;

/**
 * @author 杨森
 * @version 1.0
 * @Title:
 * @date 2020/9/4 8:49
 */
public class DataProcessing {
    private DataProcessing(){}
    public static  <T> Map<Integer, List<T>> splitList(List<T> t, int num) {
        Map<Integer, List<T>> subList = new HashMap<>(num);
        int num1 = (int) Math.floor(t.size() / num);
        for (int i = 0; i < num; i++) {
            subList.put(i, t.subList(i * num1, (i + 1) * num1));
            if (i == num - 1) {
                subList.put(i, t.subList(i * num1, t.size()));
            }
        }
        return subList;
    }
}
