package com.aaa.entity;

import com.aaa.dto.BookCatalogueDto;
import com.aaa.dto.CartoonCatalogueDto;

import java.util.List;

public class CartoonCatalogue {

    private Integer catalogueCode;

    private String catalogueName;

    private List<String> catalogueSrc;

    /**
     * 下一章code
     */
    private Integer nextCode;

    /**
     * 上一章code
     */
    private Integer upCode;

    public CartoonCatalogue(){

    }

    public Integer getNextCode() {
        return nextCode;
    }

    public void setNextCode(Integer nextCode) {
        this.nextCode = nextCode;
    }

    public Integer getCatalogueCode() {
        return catalogueCode;
    }

    public void setCatalogueCode(Integer catalogueCode) {
        this.catalogueCode = catalogueCode;
    }

    public String getCatalogueName() {
        return catalogueName;
    }

    public void setCatalogueName(String catalogueName) {
        this.catalogueName = catalogueName;
    }

    public List<String> getCatalogueSrc() {
        return catalogueSrc;
    }

    public void setCatalogueSrc(List<String> catalogueSrc) {
        this.catalogueSrc = catalogueSrc;
    }

    public Integer getUpCode() {
        return upCode;
    }

    public void setUpCode(Integer upCode) {
        this.upCode = upCode;
    }
}