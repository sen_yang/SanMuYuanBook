package com.aaa.entity;

/**
 * @author 杨森
 * @version 1.0
 * @Title: FileBook
 * @date 2020/8/19 13:36
 */
public class FileBook {
    private String bookName;
    private String fileSize;
    private Integer pageSize;

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getFileSize() {
        return fileSize;
    }

    public void setFileSize(String fileSize) {
        this.fileSize = fileSize;
    }
}
